# recipe-app-api-proxy

NGXING proxy app for our recipe app API


## Usage

### Env vars
 * `LISTEN PORT` - Port to listen on (def : `8000`)
 * `APP_HOST` - Hostname of the app (def : `app`)
 * `APP_PORT` - Port of the app to forard requests (def : `9000`)


